variable "image" {
  description = "Image ID to use"
  default     = "ami-058bd2d568351da34"
}
variable "owner" {}
variable "instance" {}

# Ubuntu 22.04 -> "ami-0b93ce03dcbcb10f6"
# Debian 12    -> "ami-058bd2d568351da34"   
